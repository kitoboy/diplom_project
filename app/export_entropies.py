import sqlite3
import time
from scipy.stats import entropy
import progressbar
import numpy 

con = sqlite3.connect('app.db')
cursor = con.cursor()

def count_entropy(el):
	file_id = el[0]
	entropy_with0 = entropy(el[1:])
	els = numpy.array(el[2:])
	sums = sum(els)
	for i in range(len(els)):
		if sums > 0:
			els[i] = els[i] / sums 
	non_z_c = numpy.count_nonzero(els)
	if non_z_c > 0:
		entropy_without0 = entropy(els)
	else:
		entropy_without0 = 10000000000
	cursor.execute("INSERT INTO entropies (file_id, entropy_with_byte0, entropy_without_byte0) VALUES ({}, {}, {})".format(file_id, entropy_with0, entropy_without0))
	con.commit()

if __name__ == '__main__':
	sql = 'SELECT DISTINCT file_id, {} FROM freq_features'.format(', '.join("byte%d"%i for i in range(0,256)))
	count = cursor.execute('SELECT COUNT(*) FROM({})'.format(sql)).fetchone()[0]
	execution = cursor.execute(sql)
	start = time.time()
	print('Start')
	bar = progressbar.ProgressBar(max_value = count - 6469)
	count = 0
	col = execution.fetchall()[6469:]
	for el in col:
		count_entropy(el)
		count += 1
		bar.update(count)

	print('time', time.time() - start, '\n\n')
