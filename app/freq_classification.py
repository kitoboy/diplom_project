import sqlite3
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import RandomForestClassifier
from modules.classification import *
import time
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score

con = sqlite3.connect('app.db')
result = Result(con)
limit = 1000000

def getScores(estimator, x, y):
    yPred = estimator.predict(x)
    return (accuracy_score(y, yPred),
        precision_score(y, yPred),
        recall_score(y, yPred),
        f1_score(y, yPred))

def my_scorer(estimator, x, y):
    a, p, r, f = getScores(estimator, x, y)
    method_name = type(estimator).__name__
    result.save(method_name, 'freq', limit * 2, accuracy=a, precision=p, recall=r, f1_score=f)
    print('Accuracy Score: ', a)
    print('Precision Score: ', p)
    print('Recall score: ',r)
    print('F1 score: ',f, '\n')
    return p

if __name__ == '__main__':
  columns = FreqColumns(con, 'freq_features')
  print('START DATA')
  data = FreqData(con, columns, limit=limit)
  print('START PARTIAL')
  partial = data.partial()

  y = partial.target()
  x = partial.data()
  print('CLASSIFICATION')

  # models = [GaussianNB(), DecisionTreeClassifier(), QuadraticDiscriminantAnalysis(), RandomForestClassifier(), SVC()]
  # names = ["Naive Bayes", "Decision Tree", 'Quadratic Discriminant Analysis', 'Random Forest', "SVM"]

  # for model, name in zip(models, names):
  #     print(name)
  #     start = time.time()
  #     scores = cross_val_score(model, x, y,scoring=my_scorer, cv=3)
  #     print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))
  #     print('time', time.time() - start, '\n\n')

  clf = RandomForestClassifier()
  clf.fit(x, y)
  import code; code.interact(local=dict(globals(), **locals()))
