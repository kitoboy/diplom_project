files = %w(
	classiffication.py
	combo_classification.py
	entropies_classification.py
	freq_classification.py
	freq_without0_classification.py
	tfs_classification.py
)

files.each do |file|
	5.times do |i|
		print file
		print ' ', i
		`python #{file}`
	end 
end