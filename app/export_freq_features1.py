from modules.feature_builder.freq_features_parser import *
from modules.file import *
import os
import sqlite3
import progressbar

BASE_PATH = 'D:\\Course-Project\\Samples\\'
VIRUS_FOLDER = 'viruses'
LEG_FOLDER = 'leg_files'

con = sqlite3.connect('app.db')

freq_table = FreqTable(con)
file_table = FileTable(con)

def parse_folder(folder):
	print('\n')
	print(folder)
	folder_path = "{}/{}".format(BASE_PATH, folder)
	files = os.listdir(folder_path)
	bar = progressbar.ProgressBar(max_value = len(files))
	idx = 0
	for file in files:
		idx += 1
		parse_file("{}/{}".format(folder_path, file), folder == VIRUS_FOLDER)
		bar.update(idx)

def parse_file(file_path, virus):
	file_id = FileParser(file_path, virus, file_table).call(True)
	if file_id:
		FreqFeaturesParser(file_path, file_id, freq_table).call()

parse_folder(VIRUS_FOLDER)
# parse_folder(LEG_FOLDER)
