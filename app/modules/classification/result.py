class Result:
	def __init__(self, con):
		self.con = con
		self.cursor = self.con.cursor()

	def save(self, method_name, feature_name, files_count, accuracy = 'NULL', precision = 'NULL', recall = 'NULL', f1_score = 'NULL'):
		sql = "INSERT INTO classification_results (method_name, feature_name, files_count, accuracy_score, precision_score, recall_score, f1_score) \
				      VALUES ('{}', '{}', {}, {}, {}, {}, {})".format(method_name, feature_name, files_count, accuracy, precision, recall, f1_score)
		self.cursor.execute(sql)	
		self.con.commit()