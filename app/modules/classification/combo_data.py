import numpy 

class ComboPartial: 
	def __init__(self, arr1, arr2):
		self.arr = numpy.vstack((numpy.array(arr1), numpy.array(arr2)))

	def target(self):
		return self.arr[0:, -1]
	
	def data(self):
		return self.arr[0:, 0:-1]

class ComboData:
	def __init__(self, con, limit = 100, with0 = True):
		self.con = con
		self.cursor = self.con.cursor()	
		self.limit = limit
		self.with0 = with0

	def sql_request(self, virus_flag):
		virus_sql = "CASE WHEN files.virus = 't' THEN 1 ELSE 0 END"

		columns = []
		if self.with0:
			tfs_table = 'tfs'
			tfids_table = 'tfidfs'
			columns.append('entropies.entropy_with_byte0')
		else:
			tfs_table = 'tfs_without0'
			tfids_table = 'tfidfs_without0'
			columns.append('entropies.entropy_without_byte0')

		pragma_columns = self.cursor.execute('PRAGMA table_info({})'.format(tfs_table)).fetchall()
		for col in pragma_columns:
			if col[1] != 'file_id':
				columns.append("{}.{}".format(tfs_table, col[1]))

		pragma_columns = self.cursor.execute('PRAGMA table_info({})'.format(tfids_table)).fetchall()
		for col in pragma_columns:
			if col[1] != 'file_id':
				columns.append("{}.{}".format(tfids_table, col[1]))

		sql = "SELECT {}, {} FROM entropies \
					   INNER JOIN {} ON {}.file_id = entropies.file_id \
					   INNER JOIN {} ON {}.file_id = entropies.file_id \
					   INNER JOIN files On entropies.file_id = files.rowid WHERE files.virus = '{}' ORDER BY random() LIMIT {}"

		sql = sql.format(','.join(columns), virus_sql, tfs_table,tfs_table, tfids_table,tfids_table, virus_flag, self.limit)
		return self.cursor.execute(sql).fetchall() 

	def partial(self):
		return ComboPartial(self.sql_request('f'), self.sql_request('t'))
