import numpy 

class TfsPartial: 
	def __init__(self, arr1, arr2):
		self.arr = numpy.vstack((numpy.array(arr1), numpy.array(arr2)))

	def target(self):
		return self.arr[0:, -1]
	
	def data(self):
		return self.arr[0:, 0:-1]

class TfsData:
	def __init__(self, con, limit = 100, with0 = True):
		self.con = con
		self.cursor = self.con.cursor()	
		self.limit = limit
		self.with0 = with0

	def sql_request(self, virus_flag):
		virus_sql = "CASE WHEN files.virus = 't' THEN 1 ELSE 0 END"

		columns = []
		if self.with0:
			tfs_table = 'tfs'
		else:
			tfs_table = 'tfs_without0'

		pragma_columns = self.cursor.execute('PRAGMA table_info({})'.format(tfs_table)).fetchall()
		for col in pragma_columns:
			if col[1] != 'file_id':
				columns.append("{}.{}".format(tfs_table, col[1]))

		sql = "SELECT {}, {} FROM {} \
					   INNER JOIN files On {}.file_id = files.rowid \
					   WHERE files.virus = '{}' ORDER BY random() LIMIT {}"

		sql = sql.format(','.join(columns), virus_sql, tfs_table, tfs_table, virus_flag, self.limit)
		return self.cursor.execute(sql).fetchall() 

	def partial(self):
		return ComboPartial(self.sql_request('f'), self.sql_request('t'))
