import numpy 

class EntropiesColumns:
	def __init__(self, con, table_name, columns):
		self.con = con
		self.cursor = self.con.cursor()
		self.table_name = table_name
		self.columns = columns

	def sql(self):
		return ','.join(self.columns)

class EntropiesPartial: 
	def __init__(self, arr1, arr2):
		self.arr = numpy.vstack((numpy.array(arr1), numpy.array(arr2)))

	def target(self):
		return self.arr[0:, -1]
	
	def data(self):
		return self.arr[0:, 0:-1]

class EntropiesData:
	def __init__(self, con, columns, limit = 100):
		self.con = con
		self.cursor = self.con.cursor()	
		self.columns = columns
		self.limit = limit

	def sql_request(self, virus_flag):
		virus_sql = "CASE WHEN files.virus = 't' THEN 1 ELSE 0 END"

		sql = 'SELECT {}, {} FROM {} INNER JOIN files On {}.file_id = files.rowid WHERE files.virus = "{}" ORDER BY random() LIMIT {}'.format(
			self.columns.sql(), virus_sql, self.columns.table_name, self.columns.table_name, virus_flag, self.limit)
		return self.cursor.execute(sql).fetchall() 

	def partial(self):
		return EntropiesPartial(self.sql_request('f'), self.sql_request('t'))
