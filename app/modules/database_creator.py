import sqlite3

class DatabaseCreator:
	def __init__(self, base_name, tables_strategy = []):
		self.base_name = base_name
		self.tables_strategy = tables_strategy
		self.connection = sqlite3.connect(base_name)
		self.cursor = self.connection.cursor()

	def process(self):
		for strategy in self.tables_strategy:
			if self.__table_not_exist(strategy.table_name):
				strategy.call(self.cursor) 
				self.connection.commit()		
	
	def __table_not_exist(self, name):
		return not bool(self.cursor.execute(self.__table_exist_sql(name)).fetchone())

	def __table_exist_sql(self, name):
		return "SELECT name FROM sqlite_master WHERE type='table' AND name='{}'".format(name)

	def __del__(self):	
		self.connection.close()

class FilesTableStrategy:
	def __init__(self):
		self.table_name = 'files'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		return 'CREATE TABLE {} (base_name VARCHAR(100), virus CHARACTER(1), \
								 content_hash VARCHAR(40), selected CHARACTER(1))'.format(self.table_name)

class DllFeaturesTableStrategy:
	def __init__(self, starting_dlls):
		self.table_name = 'dll_features'
		self.starting_dlls = starting_dlls

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		dlls_columns = ["{} CHARACTER(1) DEFAULT 'f'".format(dll) for dll in self.starting_dlls]
		columns = ['file_id INTEGER', *dlls_columns,
				   'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )

class FreqFeaturesTableStrategy:
	COLUMNS_SQL =  ', '.join("byte%d DECIMAL(1, 7)"%i for i in range(0,256))

	def __init__(self):
		self.table_name = 'freq_features'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['file_id INTEGER', self.COLUMNS_SQL,
				   'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )

class ClassificationTableStrategy:
	def __init__(self):
		self.table_name = 'classification_results'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['method_name VARCHAR(100)', 'feature_name VARCHAR(100)', 'files_count INTEGER',
				   'accuracy_score DECIMAL(1, 7)', 'precision_score DECIMAL(1,7)',
				   'recall_score DECIMAL(2,7)', 'f1_score DECIMAL(1,7)', 'timestamp DATETIME DEFAULT CURRENT_TIMESTAMP']
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )

class EntropiesTableStrategy:
	def __init__(self):
		self.table_name = 'entropies'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['file_id INTEGER', 'entropy_with_byte0', 'entropy_without_byte0', 'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )		

class TFSTableStrategy:
	COLUMNS_SQL =  ', '.join("byte%d DECIMAL(1, 7)"%i for i in range(0,256))

	def __init__(self):
		self.table_name = 'tfs'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['file_id INTEGER', self.COLUMNS_SQL,
				   'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )

class TFSWithoutZeroTableStrategy:
	COLUMNS_SQL =  ', '.join("byte%d DECIMAL(1, 7)"%i for i in range(1,256))

	def __init__(self):
		self.table_name = 'tfs_without0'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['file_id INTEGER', self.COLUMNS_SQL,
				   'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )	

class TFIDFSWithoutZeroTableStrategy:
	COLUMNS_SQL =  ', '.join("byte%d DECIMAL(1, 7)"%i for i in range(1,256))

	def __init__(self):
		self.table_name = 'tfidfs_without0'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['file_id INTEGER', self.COLUMNS_SQL,
				   'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )	

class TFIDFSTableStrategy:
	COLUMNS_SQL =  ', '.join("byte%d DECIMAL(1, 7)"%i for i in range(0,256))

	def __init__(self):
		self.table_name = 'tfidfs'

	def call(self, cursor):
		cursor.execute(self.sql())

	def sql(self):
		columns = ['file_id INTEGER', self.COLUMNS_SQL,
				   'FOREIGN KEY(file_id) REFERENCES {}(rowid)'.format(FilesTableStrategy().table_name)]
		return 'CREATE TABLE {} ({})'.format(self.table_name, ', '.join(columns) )			