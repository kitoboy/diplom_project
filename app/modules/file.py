import os
import hashlib

class FileTable:
	TABLE = 'files'

	def __init__(self, connection):
		self.con = connection
		self.cursor = self.con.cursor()

	def save_returning_rowid(self, f):
		self.cursor.execute('INSERT INTO {}(content_hash, base_name, virus) VALUES({}, {}, {})'.format(self.TABLE, f.content_hash, f.base_name, f.virus_flag))
		self.con.commit()
		return self.cursor.lastrowid

	def find_by_hash(self, hash_str):
		return self.cursor.execute('SELECT rowid FROM {} WHERE content_hash = {} LIMIT 1'.format(self.TABLE, hash_str)).fetchone()[0]
		
class FileParser:
	def __init__(self, file_path, virus, table):
		self.file_path = file_path
		self.virus = virus
		self.virus_flag = '"t"' if virus else '"f"'
		self.table = table

	def call(self, return_exists = False):
		self.base_name = '"{}"'.format(os.path.basename(self.file_path))
		self.content_hash = '"{}"'.format(self._hash())

		if self.check_by_hash(): 
			return self.table.save_returning_rowid(self)
		if return_exists:
			return self.table.find_by_hash(self.content_hash)	

	def check_by_hash(self):
		return not self.table.find_by_hash(self.content_hash)

	def _hash(self):
		sha1 = hashlib.sha1()
		with open(self.file_path, 'rb') as f:
			while True:
				data = f.read(65536)
				if not data:
					break
				sha1.update(data)
		return sha1.hexdigest()