import re

def feature_to_column(name):
	if type(name) is bytes:
		name = name.decode('utf-8')
	
	name = name.lower()
	if len(name) < 1 or name.split('.')[-1] != 'dll':
		return 
	value = re.sub('[^0-9a-z]', '_', name.split('.')[0].lower())
	if (len(value) > 1):
		return '_' + value