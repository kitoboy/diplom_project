import pefile
import os
from .helpers import feature_to_column

class DllTable:
	TABLE = 'dll_features'
	def __init__(self, connection):
		self.con = connection
		self.cursor = self.con.cursor()
		self.update_columns()

	def update_columns(self):
		pragma_columns = self.cursor.execute('PRAGMA table_info({})'.format(self.TABLE)).fetchall()
		self.columns = set([])
		for col in pragma_columns:
			self.columns.add(col[1])

	def insert_dll_features(self, file_id, dlls):
		self.check_columns(dlls)
		if len(dlls):		
			sql = 'INSERT INTO {}(file_id,{}) VALUES({}, {})'.format(self.TABLE, ','.join(dlls), file_id, ','.join(['"t"' for el in dlls])) 
		else:
			sql = 'INSERT INTO {}(file_id) VALUES({})'.format(self.TABLE, file_id)
		self.cursor.execute(sql)
		self.con.commit()

	def check_columns(self, dlls):
		diff = dlls - self.columns
		if len(diff) == 0 :
			return

		for dll in diff:
			print('{} ADD COLUMN: {}'.format(self.TABLE, dll))
			dll_sql = "{} CHARACTER(1) DEFAULT 'f'".format(dll)
			self.cursor.execute('ALTER TABLE {} ADD COLUMN {}'.format(self.TABLE, dll_sql))
		self.con.commit()
		self.update_columns()

class DllFeaturesParser:
	def __init__(self, file_path, file_id, table):
		self.file_path = file_path
		self.file_id = file_id
		self.table = table

	def call(self):
		dlls = set([])
		imports = []
		pe = None
		try:
			pe = pefile.PE(self.file_path)
			imports = pe.DIRECTORY_ENTRY_IMPORT
		except:
			if pe:
				pe.close()
			os.rename(self.file_path, self.file_path + '.bad')
			return

		for entry in imports:
			value = feature_to_column(entry.dll)

			if value:
				dlls.add(value)

		if pe:
			pe.close()	
		self.save_dll_features(dlls)
		os.rename(self.file_path, self.file_path + '.processed')

	def save_dll_features(self, dlls):
		self.table.insert_dll_features(self.file_id, dlls)
		