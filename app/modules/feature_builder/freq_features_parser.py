import pefile
import os

class FreqTable:
	TABLE = 'freq_features'
	COLUMNS_SQL =	', '.join("byte%d"%i for i in range(0,256))

	def __init__(self, connection):
		self.con = connection
		self.cursor = self.con.cursor()

	def insert(self, file_id, freqs):
		sql = "INSERT INTO {}(file_id, {}) VALUES ({}, {})".format(self.TABLE, self.COLUMNS_SQL, file_id, ', '.join(map(str, freqs)))
		self.cursor.execute(sql)
		self.con.commit()

class FreqFeaturesParser:
	def __init__(self, file_path, file_id, table):
		self.file_path = file_path
		self.file_id = file_id
		self.table = table

	def call(self):
		count = 0.0
		bytes_arr = [0] * 256
		with open(self.file_path, "rb") as f:
			text = f.read()
			count += len(text)
			for byte in text:
				bytes_arr[byte] += 1

			for i in range(256):
				bytes_arr[i] /= count

		self.save_features(bytes_arr)
		os.rename(self.file_path, self.file_path + '.freq_processed')

	def save_features(self, freqs):
		self.table.insert(self.file_id, freqs)
		