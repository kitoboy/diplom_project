from modules.database_creator import *
from modules.feature_builder.helpers import feature_to_column

data = open('starting_dlls.txt', 'r').read().split('\n')
dll_names = set([])
for dll in data:
	value = feature_to_column(dll) 
	if (value):
		dll_names.add(value)

DatabaseCreator('app.db', [FilesTableStrategy(), DllFeaturesTableStrategy(dll_names), 
						   FreqFeaturesTableStrategy(), ClassificationTableStrategy(),
						   EntropiesTableStrategy(), 
						   TFSTableStrategy(), TFSWithoutZeroTableStrategy(),
						   TFIDFSTableStrategy(), TFIDFSWithoutZeroTableStrategy()]).process()
