import sqlite3
import time
from scipy.stats import entropy
import progressbar
import numpy 
import random
from math import log
import array

con = sqlite3.connect('app.db')
cursor = con.cursor()

def count_byte_freq():
	arr = [0 for i in range(256)]
	for i in range(256):
		print(i, end=' ', flush=True)
		sql = 'SELECT COUNT(*) FROM freq_features AS ff JOIN files AS f ON f.rowid = ff.file_id WHERE ff.byte{} > 0 AND f.selected = "t"'.format(i)
		arr[i] = cursor.execute(sql).fetchone()[0]
	return arr

print("Start count bytes")
byte_freq = count_byte_freq()
sql = 'SELECT DISTINCT file_id, {} FROM freq_features AS ff JOIN files AS f ON f.rowid = ff.file_id WHERE f.selected = "t"'.format(', '.join("byte%d"%i for i in range(0,256)))
count = cursor.execute('SELECT COUNT(*) FROM({})'.format(sql)).fetchone()[0]
n = count
print(count)
execution = cursor.execute(sql)

def count_tf(el):
	file_id = el[0]
	
	tf = numpy.array(el[1:])

	tf_max = max(tf)

	if tf_max > 0:
		for i in range(len(tf)):
			tf[i] /= tf_max
			
	sql = "INSERT INTO tfs (file_id, {}) VALUES ({}, {})".format(
	', '.join("byte%d"%i for i in range(256)), 
	file_id, 
	', '.join(numpy.char.mod('%1.10f', tf))
	)
	cursor.execute(sql)

	tfidf = numpy.array(tf, copy=True)
	for i in range(len(tfidf)):
		denom = byte_freq[i] * 1.0
		if denom > 0:
			tfidf[i] *= log( n / denom)
		else:
			tfidf[i] *= log(n)

	sql = "INSERT INTO tfidfs (file_id, {}) VALUES ({}, {})".format(
	', '.join("byte%d"%i for i in range(256)), 
	file_id, 
	', '.join(numpy.char.mod('%1.10f', tfidf))
	)
	cursor.execute(sql)	

def count_tf_without0(el):
	file_id = el[0]
	
	tf = numpy.array(el[2:])

	tf_max = max(tf)

	if tf_max > 0:
		for i in range(len(tf)):
			tf[i] /= tf_max
			
	sql = "INSERT INTO tfs_without0 (file_id, {}) VALUES ({}, {})".format(
	', '.join("byte%d"%i for i in range(1, 256)), 
	file_id, 
	', '.join(numpy.char.mod('%1.10f', tf))
	)
	cursor.execute(sql)

	tfidf = numpy.array(tf, copy=True)
	for i in range(len(tfidf)):
		denom = byte_freq[i + 1] * 1.0
		if denom > 0:
			tfidf[i] *= log( n / denom)
		else:
			tfidf[i] *= log(n)

	sql = "INSERT INTO tfidfs_without0 (file_id, {}) VALUES ({}, {})".format(
	', '.join("byte%d"%i for i in range(1, 256)), 
	file_id, 
	', '.join(numpy.char.mod('%1.10f', tfidf))
	)
	cursor.execute(sql)		
		

start = time.time()
print('Start count tfs')
bar = progressbar.ProgressBar(max_value = count)
r= range(count)
count = 0
col = execution.fetchall()
for i in r:
	count_tf(col[i])
	count_tf_without0(col[i])
	con.commit()
	count += 1
	bar.update(count)
	
con.close()
print('time', time.time() - start, '\n\n')
