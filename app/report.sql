.mode csv
.separator ';'
.header on
.output report.csv
 SELECT feature_name, method_name, avg(accuracy_score), avg(precision_score), avg(recall_score), avg(f1_score) FROM classification_results WHERE files_count=10000 GROUP BY feature_name,  method_name ORder BY  feature_name, method_name;
