require 'fileutils'

DIST_FOLDER = 'C://leg_files'
# EXTS = %w(acm ax cpl dll drv efi exe mui ocx scr sys tsp)
EXTS = %w(efi exe)
DISCS = %w(C D) 

Dir.mkdir(DIST_FOLDER) unless File.exists?(DIST_FOLDER)

DISCS.each do |disc|
	EXTS.each do |ext|
		puts 
		puts ext + ' on ' + disc
		puts 'START' 
		files = Dir["#{disc}:/**/*.#{ext}"]
		puts 'FILES FIND'
		files.each_with_index do |file, idx|
			FileUtils.cp(file, "#{DIST_FOLDER}//#{File.basename(file)}#{idx}.#{ext}") if File.file?(file)
		end
		puts 'FILES COPIED'
	end
end	