import pefile
import os

PATH = 'D:\\Course-Project\\Samples\\viruses-2010-05-18'
DELETED_PATH = 'D:\\Course-Project\\Samples\\deleted-viruses' 

bad_files = []
dlls = set([])

files = os.listdir(PATH)
for file in files:
	file_path = PATH + "\\" + file
	try:
		pe = pefile.PE(file_path)

		for entry in pe.DIRECTORY_ENTRY_IMPORT:
	  		dlls.add(entry.dll)
	except:
		try:
			os.rename(file_path, DELETED_PATH + '\\' + file)
		except:
			bad_files.append(file_path)	

print('\n')
print('\n')
print('BAD FILES:\n')
for file in bad_files:
	print(file)

print('\n')
print('\n')
print('DLLS:\n')
for file in dlls:
	print(file)	
