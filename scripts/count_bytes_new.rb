require 'csv'
require 'pry'

report_file_path = ARGV.first()
files = ARGV[1..-1]

CSV.open(report_file_path, 'w', col_sep: ';') do |csv|
	files.each_with_index do |file, index|
		data = Array.new(257, 0)
		
		puts file
		s = File.read(file) 
		s.each_byte do |byte|
			data[byte] += 1
		end
		size = s.size
		data.map! { |el| el.to_f / size }

		data[256] = File.basename(file)
		csv << data
	end		
end
